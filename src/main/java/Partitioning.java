import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

public class Partitioning {

    public static void main(String[] args) throws IOException {
        try {
            String url = "jdbc:postgresql://localhost:14432/postgres";
            String user = "postgres";
            String password = "postgres";
            Class.forName("org.postgresql.Driver");

            Connection connection = DriverManager.getConnection(url, user, password);

            // Without partitioning

            String selectQuery =
                    "SELECT COUNT(*) AS Sum"
                            + " FROM emp_rating " +
                            " WHERE r=5";
            PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);

            long startTime = System.currentTimeMillis();
            ResultSet resultSet = preparedStatement.executeQuery();
            long endTime = System.currentTimeMillis();
            System.out.println("1 without partitioning: " + (endTime - startTime));

            while (resultSet.next()) {
                int sum = resultSet.getInt("Sum");
                String res = "sum: " + sum;
                System.out.println(res);
            }

            resultSet.close();
            preparedStatement.close();

            // Without partitioning

            selectQuery =
                    "SELECT COUNT(*) AS Sum"
                            + " FROM emp_rating_demo" +
                            " WHERE r=5";
            preparedStatement = connection.prepareStatement(selectQuery);

            startTime = System.currentTimeMillis();
            resultSet = preparedStatement.executeQuery();
            endTime = System.currentTimeMillis();
            System.out.println("1 with partitioning: " + (endTime - startTime));

            while (resultSet.next()) {
                int sum = resultSet.getInt("Sum");
                String res = "sum: " + sum;
                System.out.println(res);
            }

            resultSet.close();
            preparedStatement.close();

            // Range with partition

            selectQuery =
                    "SELECT COUNT(*) AS Sum"
                            + " FROM emp_rating_demo" +
                            " WHERE r>3 AND r<7";
            preparedStatement = connection.prepareStatement(selectQuery);

            startTime = System.currentTimeMillis();
            resultSet = preparedStatement.executeQuery();
            endTime = System.currentTimeMillis();
            System.out.println("range with partitioning: " + (endTime - startTime));

            while (resultSet.next()) {
                int sum = resultSet.getInt("Sum");
                String res = "sum: " + sum;
                System.out.println(res);
            }

            resultSet.close();
            preparedStatement.close();

            // Range without partition

            selectQuery =
                    "SELECT COUNT(*) AS Sum"
                            + " FROM emp_rating" +
                            " WHERE r>3 AND r<7";
            preparedStatement = connection.prepareStatement(selectQuery);

            startTime = System.currentTimeMillis();
            resultSet = preparedStatement.executeQuery();
            endTime = System.currentTimeMillis();
            System.out.println("range without partitioning: " + (endTime - startTime));

            while (resultSet.next()) {
                int sum = resultSet.getInt("Sum");
                String res = "sum: " + sum;
                System.out.println(res);
            }

            resultSet.close();
            preparedStatement.close();

            // Range with partition

            selectQuery =
                    "SELECT COUNT(*) AS Sum"
                            + " FROM emp_rating_demo" +
                            " WHERE r>0 AND r<10";
            preparedStatement = connection.prepareStatement(selectQuery);

            startTime = System.currentTimeMillis();
            resultSet = preparedStatement.executeQuery();
            endTime = System.currentTimeMillis();
            System.out.println("all range with partitioning: " + (endTime - startTime));

            while (resultSet.next()) {
                int sum = resultSet.getInt("Sum");
                String res = "sum: " + sum;
                System.out.println(res);
            }

            resultSet.close();
            preparedStatement.close();

            // Range without partition

            selectQuery =
                    "SELECT COUNT(*) AS Sum"
                            + " FROM emp_rating" +
                            " WHERE r>0 AND r<10";
            preparedStatement = connection.prepareStatement(selectQuery);

            startTime = System.currentTimeMillis();
            resultSet = preparedStatement.executeQuery();
            endTime = System.currentTimeMillis();
            System.out.println("all range without partitioning: " + (endTime - startTime));

            while (resultSet.next()) {
                int sum = resultSet.getInt("Sum");
                String res = "sum: " + sum;
                System.out.println(res);
            }

            resultSet.close();
            preparedStatement.close();

            // ALL Range without partition

            selectQuery =
                    "SELECT COUNT(*) AS Sum"
                            + " FROM emp_rating";
            preparedStatement = connection.prepareStatement(selectQuery);

            startTime = System.currentTimeMillis();
            resultSet = preparedStatement.executeQuery();
            endTime = System.currentTimeMillis();
            System.out.println("without partitioning: " + (endTime - startTime));

            while (resultSet.next()) {
                int sum = resultSet.getInt("Sum");
                String res = "sum: " + sum;
                System.out.println(res);
            }

            resultSet.close();
            preparedStatement.close();

            // ALL Range with partition

            selectQuery =
                    "SELECT COUNT(*) AS Sum"
                            + " FROM emp_rating_demo";
            preparedStatement = connection.prepareStatement(selectQuery);

            startTime = System.currentTimeMillis();
            resultSet = preparedStatement.executeQuery();
            endTime = System.currentTimeMillis();
            System.out.println("with partitioning: " + (endTime - startTime));

            while (resultSet.next()) {
                int sum = resultSet.getInt("Sum");
                String res = "sum: " + sum;
                System.out.println(res);
            }

            resultSet.close();
            preparedStatement.close();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.err.println("PostgreSQL JDBC driver not found");
        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println("Database connection error");
        }
    }
}
